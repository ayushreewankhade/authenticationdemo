package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.example.demo.interceptor.ApiLogger;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer{

	@Autowired
	ApiLogger interceptor;
	
	 @Override
	 public void addInterceptors(InterceptorRegistry registry) {
	  // this interceptor will be applied to all URLs
	  registry.addInterceptor(interceptor).addPathPatterns("/**");
	 }
	 
	
//	 @Override
//		public void addResourceHandlers(ResourceHandlerRegistry registry) {
//
//			registry.addResourceHandler("/public/**").addResourceLocations("classpath:/public/").setCacheControl(CacheControl.noCache());
//
//		}
}
