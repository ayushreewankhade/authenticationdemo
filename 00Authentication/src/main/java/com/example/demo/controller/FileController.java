package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.ErrorResponseDto;
import com.example.demo.dto.SuccessResponseDto;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.FileUploadEntity;
import com.example.demo.service.FileStorageService;

@RestController
@RequestMapping("/file")
public class FileController {

	@Autowired
	private FileStorageService fileStorageService;

	@PostMapping("/upload-file")
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file, @RequestParam(defaultValue = "") String type, HttpServletRequest request) {

		FileUploadEntity fileDetail = new FileUploadEntity();

		try {

			fileDetail = fileStorageService.storeFile(file, fileStorageService.getFolderName(type), request);

		} catch (ResourceNotFoundException e) {

			return new ResponseEntity<>(new ErrorResponseDto(e.getMessage(), "invalidUploadType"), HttpStatus.BAD_REQUEST);

		}
		 return new ResponseEntity<>(new SuccessResponseDto("File Uploaded Successfully", "fileUploadSuccessfully", new UploadFileResponse(fileDetail.getId(), fileDetail.getFilename(), type)), HttpStatus.CREATED);

	}}
