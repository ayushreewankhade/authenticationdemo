package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.JobApplicationDto;
import com.example.demo.dto.LoginRequest;
import com.example.demo.model.JobApplication;
import com.example.demo.model.JwtUser;
import com.example.demo.model.LoggerEntity;
import com.example.demo.repo.JobApplicationRepo;
import com.example.demo.repo.LoggerRepository;
import com.example.demo.service.JobApplicationService;
import com.example.demo.util.JwtUtil;


@RestController
public class JobsAppliedController {

	@Autowired
	JobApplicationRepo jobApplicationRepo;
	
	@Autowired
	JobApplicationService jobApplicationService;
	
	@Autowired
	JwtUtil jwtUtil;
	
	@Autowired
	LoggerRepository loggerRepository;
	
	/*@GetMapping("/jobs-applied")
	public List<JobApplication> getJobs () {
		List<JobApplication> list = jobApplicationRepo.findAll();
		List<JobApplication> jobAppliedList = new ArrayList<>();
		list.forEach(c -> {
			JobApplication applied = new JobApplication();
			applied.setAppliedDate(c.getAppliedDate());
			applied.setId(c.getId());
			applied.setJob(c.getJob());
			applied.setJwtUser(c.getJwtUser());
		});
		return jobAppliedList;
	}*/

	@GetMapping("/jobs-applied")
    public List<JobApplication> getPaginatedJobs(@RequestParam (required = false,defaultValue = "0") int pageNo, 
    		@RequestParam(required = false,defaultValue = "10") int pageSize,@RequestHeader("Authorization") String token) {
		System.out.println(token);
		
		String temp = token.split(" ")[1];
		String username = jwtUtil.extractUsername(temp);
		//LoggerEntity logger = loggerRepository.findByToken(token);
        return jobApplicationService.findPaginatedJobsApplied(pageNo, pageSize, username);
    
	}
	
}
