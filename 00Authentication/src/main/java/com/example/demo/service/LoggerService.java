package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.LoggerDto;
import com.example.demo.model.JwtUser;
import com.example.demo.model.LoggerEntity;
import com.example.demo.repo.LoggerRepository;

@Service
public class LoggerService implements ILoggerService {

	@Autowired
	private LoggerRepository loggerRepository;

	
	public void createLogger(LoggerDto loggerDto, JwtUser jwtuser) {
		LoggerEntity logger=new LoggerEntity();
		//logger.setId(user);
		logger.setToken(loggerDto.getToken());
		logger.setExpireAt(loggerDto.getExpireAt());
		loggerRepository.save(logger);

	}
	
     public void logout(String token) {
		
		final String token1=token.substring(7);

		loggerRepository.removeByToken(token);


	}
    //@Transactional
	//public void logout(String token, Integer userId, String email) {
    //final String userToken = token;
    //cache.removeKeyFromCache(userToken);
    //cache.removeKeyFromCache(userId + "permission");
	//cache.removeKeyFromCache(email);
	//loggerRepository.removeByToken(userToken);
	//}
	
     @Override
 	public LoggerEntity getLoggerDetail(String token) {
    	 LoggerEntity logger = loggerRepository.findByToken(token);
 		 return logger;
 	}
     
//	@Override
//	public LoggerEntity getLoggerDetail(String token) {
//
//		LoggerEntity logger;
//
//		if (!cache.isKeyExist(token, token)) {
//
//			logger = loggerRepository.findByToken(token);
//			cache.addInCache(token, token, logger);
//
//		} else {
//
//			logger = (LoggerEntity) cache.getFromCache(token, token);
//
//		}
//
//		return logger;// loggerRepository.findByToken(token);
//
//	}
}
