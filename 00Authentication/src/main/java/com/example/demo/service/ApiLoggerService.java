package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.ApiLoggerEntity;
import com.example.demo.repo.ApiLoggerRepository;

@Service("apiLoggerServiceImpl")
public class ApiLoggerService {

		public ApiLoggerService() {

			// TODO Auto-generated constructor stub
		}

		@Autowired
		private ApiLoggerRepository apiLoggerRepository;

		public void createApiLog(ApiLoggerEntity api) {

			apiLoggerRepository.save(api);

		}

	}

