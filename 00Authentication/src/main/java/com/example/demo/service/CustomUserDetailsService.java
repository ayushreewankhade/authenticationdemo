package com.example.demo.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ResetPasswordDto;
import com.example.demo.exception.ErrorResponseDto;
import com.example.demo.model.JwtUser;
import com.example.demo.repo.JwtUserRepository;
import com.example.demo.util.PasswordValidator;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	private static final long EXPIRE_TOKEN_AFTER_MINUTES = 15;
	
    @Autowired
    JwtUserRepository jwtUserRepository;
    
    @Autowired
	private PasswordEncoder bcryptEncoder;
  
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        JwtUser jwtUser = jwtUserRepository.findUserByEmail(email);
        if (jwtUser == null) {
            throw new UsernameNotFoundException("email Not found" + email);
        }
        return new User(jwtUser.getEmail(), jwtUser.getPassword(), new ArrayList<>());
    }

    	public String forgotPassword(String email) {

    		Optional<JwtUser> userOptional = Optional
    				.ofNullable(jwtUserRepository.findByEmail(email));

    		if (!userOptional.isPresent()) {
    			return "Invalid email id.";
    		}

    		JwtUser jwtUser = userOptional.get();
    		jwtUser.setToken(generateToken());
    		jwtUser.setTokenCreationDate(LocalDateTime.now());

    		jwtUser = jwtUserRepository.save(jwtUser);

    		return jwtUser.getToken();
    	}

    	public String resetPassword(ResetPasswordDto body) { //, String confirmPassword

    		if (body.getPassword().equals(body.getConfirmPassword())) {
    		
    		Optional<JwtUser> jwtUserOptional = Optional
    				.ofNullable(jwtUserRepository.findByToken(body.getToken()));

    		if (!jwtUserOptional.isPresent()) {
    			return "Invalid token.";
    		}
			
//    		if (body.getPassword().equals(body.getConfirmPassword())) {
//    			return "new Password and Confirm Password must be same";
//    		}

    		LocalDateTime tokenCreationDate = jwtUserOptional.get().getTokenCreationDate();

    		if (isTokenExpired(tokenCreationDate)) {
    			return "Token expired.";

    		}
    	
    		JwtUser jwtUser = jwtUserOptional.get();

    		jwtUser.setPassword(bcryptEncoder.encode(body.getPassword()));
    		jwtUser.setToken(body.getToken());
    		jwtUser.setTokenCreationDate(null);

    		jwtUserRepository.save(jwtUser);

    		return "Your password successfully updated.";
    	}return "new Password and Confirm Password must be same";
		}

    	/*
    	 * Generate unique token.
    	 * 
    	 * @return unique token
    	 */
    	private String generateToken() {
    		StringBuilder token = new StringBuilder();

    		return token.append(UUID.randomUUID().toString())
    				.append(UUID.randomUUID().toString()).toString();
    	}

    	/*
    	 * Check whether the created token expired or not.
    	 * 
    	 * @param tokenCreationDate
    	 * @return true or false
    	 */
    	private boolean isTokenExpired(final LocalDateTime tokenCreationDate) {

    		LocalDateTime now = LocalDateTime.now();
    		Duration diff = Duration.between(tokenCreationDate, now);

    		return diff.toMinutes() >= EXPIRE_TOKEN_AFTER_MINUTES;
    	}
    	
    	public Boolean comparePassword(String password, String hashPassword) 
    	{
    		return bcryptEncoder.matches(password, hashPassword);
    	}

		public JwtUser FindByEmail(String usernameOrEmail) {
		
			JwtUser user =this.jwtUserRepository.findByEmail(usernameOrEmail);
			return  user;

		}
		
		public JwtUser fingById(int id) {
			
			JwtUser user =this.jwtUserRepository.getReferenceById(id);
			return  user;

		}

		
}
		
