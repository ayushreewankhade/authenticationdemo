package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.dto.JobDto;
import com.example.demo.model.Job;
import com.example.demo.model.JwtUser;
import com.example.demo.repo.JobRepo;
import com.example.demo.repo.JwtUserRepository;

import io.jsonwebtoken.ExpiredJwtException;

@Service
public class JobService {

	@Autowired
	private JobRepo jobRepo;
	
	@Autowired
	JwtUserRepository jwtUserRepo;

	 public List<Job> findPaginated(int pageNo, int pageSize) throws ExpiredJwtException {

	        Pageable paging = PageRequest.of(pageNo, pageSize,Sort.by("postedDate").ascending());
	        Page<Job> pagedResult = jobRepo.findAll(paging);

	        return pagedResult.toList();
	    }
	 public Job postJob(JobDto jobdto) {
			Job job = new Job();
		    job.setActive(true);
			job.setJobTitle(jobdto.getJobTitle());
		    job.setJobDescription(jobdto.getJobDescription());
			return jobRepo.save(job);
	 }	
	 
	 public Job fingById(long id) {
			
			Job job =this.jobRepo.getReferenceById(id);
			return  job;

		}
}
