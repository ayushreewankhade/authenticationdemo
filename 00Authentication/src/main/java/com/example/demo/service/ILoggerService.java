package com.example.demo.service;

import com.example.demo.model.LoggerEntity;

public interface ILoggerService {

	LoggerEntity getLoggerDetail(String token);
}
