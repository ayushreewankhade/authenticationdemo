package com.example.demo.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Job {

	@Id
	@Column(name = "job_id", updatable = false)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "job_seq")
	//@SequenceGenerator(name = "job_seq", sequenceName = "job_seq", allocationSize = 1)
	private Long id;

	private String jobTitle;

	private String jobDescription;

	@ManyToOne(targetEntity = Recruiter.class, cascade = { CascadeType.MERGE, CascadeType.REFRESH })
	//@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "recruiter_id")
	private Recruiter postedBy;

	private LocalDate postedDate = LocalDate.now(ZoneId.of("GMT+05:30"));

	@JsonIgnore
	@OneToMany(mappedBy = "job", targetEntity = JobApplication.class, cascade = { CascadeType.MERGE,
			CascadeType.REFRESH })
	//@OnDelete(action = OnDeleteAction.CASCADE)
	private List<JobApplication> jobApplications;

	private Boolean active;

	public Job() {
		super();
	}

	public Job(Long id, String jobTitle, String jobDescription, LocalDate postedDate,
			JwtUser awardedTo, /*List<JobApplication> jobApplications,*/ Boolean active) {
		super();
		this.id = id;
		this.jobTitle = jobTitle;
		this.jobDescription = jobDescription;
		this.postedDate = postedDate;
		//this.jobApplications = jobApplications;
		this.active = active;
	}

	public Boolean getActive() {
		return active;
	}

	public Long getId() {
		return id;
	}

	public List<JobApplication> getJobApplications() {
		return jobApplications;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public LocalDate getPostedDate() {
		return postedDate;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setJobApplications(List<JobApplication> jobApplications) {
		this.jobApplications = jobApplications;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public void setPostedDate(LocalDate postedDate) {
		this.postedDate = postedDate;
	}

}
