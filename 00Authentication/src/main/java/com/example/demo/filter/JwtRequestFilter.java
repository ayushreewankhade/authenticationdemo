package com.example.demo.filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.example.demo.config.MultiReadHttpServletRequest;
import com.example.demo.service.CustomUserDetailsService;
import com.example.demo.util.JwtUtil;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter{

	@Autowired
    JwtUtil jwtUtil;

    @Autowired
    CustomUserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        final String authorizationHeader =  request.getHeader("Authorization");
        MultiReadHttpServletRequest multiReadRequest = new MultiReadHttpServletRequest(request);
        String jwtToken = null;
        String username = null;
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            jwtToken = bearerToken.substring(7, bearerToken.length());
            try {
            username = jwtUtil.extractUsername(jwtToken);
            }catch (IllegalArgumentException e) {
				System.out.println("Unable to get JWT Token");
			} catch (ExpiredJwtException e) {
				System.out.println("JWT Token has expired");
			}
		} else {
			logger.warn("JWT Token does not begin with Bearer String");
		}

            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                if (jwtUtil.validateToken(jwtToken, userDetails)) {
                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                }
            }
        filterChain.doFilter(multiReadRequest, response);
    }}
    /*//1.get token--from header
    			//key authoriaztion 

    			final String requestToken=request.getHeader("Authorization"); 

    			//2 .token start from Bearer 23434ghfd


    			System.out.println(requestToken);  

    			String email=null;
    			String token=null;
    		//	JsonObject jsonObj = null;
    			if(requestToken !=null && requestToken.startsWith("Bearer "))
    			{
    				token =requestToken.substring(7);

    				try {
    					System.out.println("TOKEN "+token);
    					email = this.jwtUtil.extractUsername(token);
    					//jsonObj = JsonParser.parseString(email).getAsJsonObject();
    					
    				}	catch(IllegalArgumentException e)
    				{
    	 
    					System.out.println(" Unable to get Jwt token");
    				}
    				catch(ExpiredJwtException e)
    				{

    					System.out.println("Jwt token has expired !!!!!!!!!!");
    				}
    				catch(IllegalStateException e) 
    				{
    					System.out.println("Cant not convert to JSON");
    				}
    				catch(NullPointerException e)
    				{
    					System.out.println("Null Value Not Allowed...");
    				}
    			}
    			else 
    			{
    				logger.warn("JWT token  Dose not start with Bearer ");
    			}

    			//once we get token token ,now validate

    			if(email != null && SecurityContextHolder.getContext().getAuthentication() ==null)
    			{
    				
    				UserDetails userDetails=this.userDetailsService.loadUserByUsername(email);    ///loadUserByUsername(jsonObj.get("email").getAsString());             //loadUserByUsername(email);
    				
    				//validate need username detials to use userdetails service 
    				if(this.jwtUtil.validateToken(token, userDetails))
    				{

    					//set authetication ---create authi object need --create userpass
    					//one type authication 
    					UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken=new UsernamePasswordAuthenticationToken(userDetails,null, userDetails.getAuthorities());
    					usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetails(request));
    					SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

    				}
    			}

    			filterChain.doFilter(request, response);


    		}*/

