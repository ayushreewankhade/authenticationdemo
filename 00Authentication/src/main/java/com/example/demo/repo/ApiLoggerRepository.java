package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.ApiLoggerEntity;

public interface ApiLoggerRepository extends JpaRepository<ApiLoggerEntity, Long> {
	
}
