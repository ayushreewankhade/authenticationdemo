/*package com.example.demo.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Recruiter;

@Repository
public interface RecruiterRepo extends JpaRepository<Recruiter, Integer> {

    List<Recruiter> findAll();

    Recruiter findByEmail(String email);

    Recruiter findUserByEmail(String email);

    Recruiter findByToken(String token);

	boolean findUserByEmail(boolean matches);
}
*/