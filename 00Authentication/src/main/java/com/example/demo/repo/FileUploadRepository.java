package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.FileUploadEntity;

public interface FileUploadRepository extends JpaRepository<FileUploadEntity, Integer> {
}
